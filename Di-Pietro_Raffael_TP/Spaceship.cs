﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP
{
    internal abstract class Spaceship: Models.ISpaceship
    {
        public string Name { get; set; }
        public double Structure { get; set; }
        public double Shield { get; set; }
        public bool IsDestroyed { get; }
        public int MaxWeapons { get; }
        public List<Weapon> Weapons { get; }
        public double AverageDamages {
            get 
            {
                double sum = 0;
                this.Weapons.ForEach((weapon) =>
                {
                    sum += (weapon.MinDamage + weapon.MaxDamage) / 2;
                });

                return sum;
            }
        }
        public double CurrentStructure { get; set; }
        public double CurrentShield { get; set; }
        public bool BelongsPlayer { get; }





        public Spaceship(string name, int maxShield, int maxStructure)
        {
            this.Weapons = new List<Weapon>();
            this.Shield = maxShield;
            this.Structure = maxStructure;
            this.Name = name;

            this.CurrentShield = maxShield;
            this.CurrentStructure = maxStructure;

            this.MaxWeapons = 3;

            this.IsDestroyed = maxShield <= 0 && maxStructure <= 0;
        }

        public virtual void TakeDamages(double damages)
        {
            this.HurtSpaceship(damages);
        }

        protected void HurtSpaceship(double damages)
        {
            this.CurrentShield -= damages;
            if (this.CurrentShield <= 0)
            {
                double damageOverload = Math.Abs(this.CurrentShield);
                this.CurrentStructure -= damageOverload;
            }
        }

        public void RepairShield(double repair)
        {
            this.CurrentShield += repair;
        }

        public virtual void ShootTarget(Spaceship target)
        {
            Random random = new Random();
            target.TakeDamages(this.Weapons[random.Next(0, this.MaxWeapons)].Shoot());
        }

        public void ReloadWeapons()
        {

        }

        public void AddWeapon(Weapon weapon)
        {
            if(this.Weapons.Count >= this.MaxWeapons)
            {
                return;
            }

            if (!Armory.GetInstance().weapons.Contains(weapon))
            {
                throw new ArmoryException(weapon.Name);
            }

            this.Weapons.Add(weapon);
        }

        public void RemoveWeapon(Weapon oWeapon)
        {
            this.Weapons.Remove(oWeapon);
        }

        public void ClearWeapons()
        {
            this.Weapons.Clear();
        }

        public void ViewWeapons()
        {
            Console.WriteLine("== Armes du vaisseau "+ this.Name +"==");
            this.Weapons.ForEach(weapon =>
            {
                Console.WriteLine(weapon.ToString());
            });
            Console.WriteLine("====");
        }

        public void ViewShip()
        {

        }

        //public double AverageDamages()
        //{
        //    double sum = 0;
        //    this.weapons.ForEach((weapon) =>
        //    {
        //        sum += (weapon.minDamage + weapon.maxDamage) / 2;
        //    });

        //    return sum;
        //}

        public override string ToString()
        {
            return this.Name + " | Shield: " + this.CurrentShield + "/" + this.Shield + " | Structure: " + this.CurrentStructure + "/" + this.Structure;
        }
    }
}
