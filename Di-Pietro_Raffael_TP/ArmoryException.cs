﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP
{
    internal class ArmoryException : Exception
    {
        public ArmoryException(string weaponName): base(String.Format("Vous ne pouvez pas ajouter une arme qui n'est pas dans l'armurerie. Nom arme : {0}", weaponName))
        {}
    }
}
