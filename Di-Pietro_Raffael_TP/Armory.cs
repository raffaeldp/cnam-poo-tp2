﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP
{
    internal sealed class Armory
    {
        private static Armory instance;
        public readonly List<Weapon> weapons = new List<Weapon>();

        public static Armory GetInstance()
        {
            if (instance == null)
            {
                instance = new Armory();
            }
            return instance;
        }

        private Armory()
        {
            this.Init();
        }

        private void Init()
        {
            this.AddWeapon(new Weapon("laser", 50, 100, EWeaponType.Direct, 3));
            this.AddWeapon(new Weapon("tête chercheuse", 20, 50, EWeaponType.Guided, 2));
            this.AddWeapon(new Weapon("grenade", 30, 40, EWeaponType.Explosive, 3));
            this.AddWeapon(new Weapon("Maximator", 30, 40, EWeaponType.Explosive, 2));
            this.AddWeapon(new Weapon("Laser", 2, 3, EWeaponType.Direct, 2));
            this.AddWeapon(new Weapon("Hammer", 1, 8, EWeaponType.Explosive, 1.5));
            this.AddWeapon(new Weapon("Torpille", 3, 3, EWeaponType.Guided, 2));
            this.AddWeapon(new Weapon("Mitrailleuse", 6, 8, EWeaponType.Direct, 1));
            this.AddWeapon(new Weapon("EMG", 1, 7, EWeaponType.Explosive, 1.5));
            this.AddWeapon(new Weapon("Missile", 4, 100, EWeaponType.Guided, 4));
        }

        public Weapon GetWeaponByName(string name)
        {
            return this.weapons.Find(weapons => weapons.Name == name);
        }

        public void ViewArmory()
        {
            Console.WriteLine("== Armes de l'armurerie ==");
            this.weapons.ForEach(weapon =>
            {
                Console.WriteLine(weapon.ToString());
            });
            Console.WriteLine("====");
        }

        public void AddWeapon(Weapon weapon)
        {
            this.weapons.Add(weapon);
        }

        public void RemoveWeapon(Weapon oWeapon)
        {
            this.weapons.Remove(oWeapon);
        }

        public void ClearWeapons()
        {
            this.weapons.Clear();
        }
    }
}
