﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP
{
    public enum EWeaponType
    {
        Direct,
        Explosive,
        Guided,
    }
}
