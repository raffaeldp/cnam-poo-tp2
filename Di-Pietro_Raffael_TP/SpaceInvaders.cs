﻿using System;
using System.Collections.Generic;
namespace Di_Pietro_Raffael_TP
{
    internal class SpaceInvaders
    {

        List<Spaceship> enemies = new List<Spaceship>();
        Player player;

        public SpaceInvaders()
        {
            this.Init();

            while (!this.player.defaultSpaceShip.IsDestroyed && !isAllSpaceshipDestroyed())
            {
                this.PlayRound();
            }
        }

        static void Main()
        {
            SpaceInvaders spaceInvaders = new SpaceInvaders()
        }

        private bool isAllSpaceshipDestroyed()
        {
            bool isAllSpaceshipDestroyed = true;
            this.enemies.ForEach(e =>
            {
                if (e.IsDestroyed == false)
                {
                    isAllSpaceshipDestroyed = false;
                }
            });
            return isAllSpaceshipDestroyed;
        }

        private void PlayRound()
        {
            // Repair enemies & player
            this.enemies.ForEach(enemy =>
            {
                if(enemy.CurrentShield < enemy.Shield)
                {
                    Random random = new Random();
                    enemy.RepairShield(random.Next(1, 2));
                }
            });
            if(player.defaultSpaceShip.CurrentShield < player.defaultSpaceShip.Shield)
            {
                Random rnd = new Random();
                player.defaultSpaceShip.RepairShield(rnd.Next(1, 2));
            }

            // All enemies shoot
            this.enemies.ForEach(enemy =>
            {
                enemy.ShootTarget(player.defaultSpaceShip);
            });

            Random random = new Random();
            this.player.defaultSpaceShip.ShootTarget(this.enemies[random.Next(0, this.enemies.Count() - 1)]);
        }

        private void Init()
        {
            this.player = new Player("Jean", "machin", "truc");
            this.enemies.Add(new Spaceships.Dart());
            this.enemies.Add(new Spaceships.BWing());
            this.enemies.Add(new Spaceships.Rocinante());
        }
    }
}
