﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP
{
    internal class Weapon: Models.IWeapon
    {
        public string Name { get; set; }
        public EWeaponType Type { get; set; }
        public double MinDamage { get; set; }
        public double MaxDamage { get; set; }
        public double AverageDamage { get; }

        public double ReloadTime { get; set; }
        public double TimeBeforReload { get; set; }
        public bool IsReload { get; }

        public Weapon(String name, int minDamage, int maxDamage, EWeaponType type, double reloadTime)
        {
            this.Name = name;
            this.MinDamage = minDamage;
            this.MaxDamage = maxDamage;
            this.Type = type;

            this.ReloadTime = reloadTime;
            this.TimeBeforReload = this.ReloadTime;
        }

        public double Shoot()
        {
            this.TimeBeforReload--;
            double shootDamages = 0;

            Random random = new Random();
            double damages = random.NextDouble() * (this.MaxDamage - this.MinDamage) + this.MinDamage;
            if(this.TimeBeforReload == 0)
            {
                switch(this.Type)
                {
                    case EWeaponType.Direct:
                    {
                        shootDamages = random.Next(0, 9) == 0 ? 0 : damages;
                        break;
                    };
                    case EWeaponType.Explosive:
                    {
                        shootDamages = random.Next(0, 3) == 0 ? 0 : (damages + this.ReloadTime) * 2;
                        break;
                    };
                    case EWeaponType.Guided:
                    {
                        shootDamages = this.MinDamage;
                        break;
                    };
                    default: shootDamages = 0; break;
                }
            }

            return shootDamages;
        }

        public override String ToString()
        {
            return this.Name + " (max-dégats: " + this.MaxDamage + " min-dégats: " + this.MinDamage + ")";
        }

        public override bool Equals(object? obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj.GetType() != typeof(Weapon))
            {
                return false;
            }
            return this.Name == ((Weapon)obj).Name;
        }
    }
}
