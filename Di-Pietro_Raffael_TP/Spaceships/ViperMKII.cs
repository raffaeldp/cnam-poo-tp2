﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP.Spaceships
{
    internal class ViperMKII : Spaceship
    {
        public ViperMKII(): base("ViperMKII", 15, 10) {
            AddWeapon(Armory.GetInstance().GetWeaponByName("Mitrailleuse"));
            AddWeapon(Armory.GetInstance().GetWeaponByName("EMG"));
            AddWeapon(Armory.GetInstance().GetWeaponByName("Missile"));
        }

        public override void TakeDamages(double damages)
        {
            Random random = new Random();
            if (random.Next(0, 1) == 1)
            {
                this.HurtSpaceship(damages);
            }
        }
    }
}
