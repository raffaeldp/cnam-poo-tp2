﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP.Spaceships
{
    internal class Dart: Spaceship
    {
        public Dart(): base("Dart", 3, 10) {
            AddWeapon(Armory.GetInstance().GetWeaponByName("Laser"))
        }

        public override void ShootTarget(Spaceship target)
        {
            Random random = new Random();
            int shootingWeaponIndex = random.Next(0, this.MaxWeapons);
            target.TakeDamages(this.Weapons[shootingWeaponIndex].Shoot());
            if(this.Weapons[shootingWeaponIndex].Type == EWeaponType.Direct)
            {
                this.Weapons[shootingWeaponIndex].TimeBeforReload = 0;
            }
        }
    }
}
