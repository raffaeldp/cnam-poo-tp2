﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP.Spaceships
{
    internal class BWing: Spaceship
    {
        public BWing(): base("B-Wings", 0, 30) {
            AddWeapon(Armory.GetInstance().GetWeaponByName("Hammer"));

        }

        public override void ShootTarget(Spaceship target)
        {
            Random random = new Random();
            int shootingWeaponIndex = random.Next(0, this.MaxWeapons);
            target.TakeDamages(this.Weapons[shootingWeaponIndex].Shoot());
            if(this.Weapons[shootingWeaponIndex].Type == EWeaponType.Explosive)
            {
                this.Weapons[shootingWeaponIndex].TimeBeforReload = 0;
            }
        }
    }
}
