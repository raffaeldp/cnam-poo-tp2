﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Di_Pietro_Raffael_TP.Spaceships
{
    internal class Rocinante: Spaceship
    {
        public Rocinante(): base("Rocinante", 5, 3) {
            AddWeapon(Armory.GetInstance().GetWeaponByName("Torpille"));
        }

        public override void TakeDamages(double damages)
        {
            Random random = new Random();
            if (random.Next(0, 1) == 1)
            {
                this.HurtSpaceship(damages);
            }
        }
    }
}
